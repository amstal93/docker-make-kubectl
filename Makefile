DOCKER_NAMESPACE ?= julienlecomte
DOCKER_IMAGE     := docker-make-kubectl
DOCKER_TAG       ?= $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)

# git says 'master', docker wants 'latest'
ifeq ($(DOCKER_TAG),master)
DOCKER_TAG := latest
endif

DOCKER_LOCAL ?= $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE)

#-------------------------------------------------------------------------------
# Google Container Tools 'container-structure-test'
#
# https://github.com/GoogleContainerTools/container-structure-test
UT_IMAGE ?= gcr.io/gcp-runtimes/container-structure-test
UTFLAGS  ?= --quiet

#-------------------------------------------------------------------------------
.DEFAULT_GOAL:= all
.PHONY: all help check

all: image

help: ## Show this help
	@echo "Available targets:"
	@grep '^[a-z]' $(MAKEFILE_LIST) | sort | awk -F ':.*?## ' 'NF==2 {printf "  %-26s%s\n", $$1, $$2}'

check: image-check ## run unit-tests

#-------------------------------------------------------------------------------
.PHONY: image image-check pull push image-name

image:
	docker build $(DOCKERFLAGS) --tag $(DOCKER_LOCAL):$(DOCKER_TAG) .

image-check: image
	-docker pull $(UT_IMAGE)
	docker run --rm \
	  -v /var/run/docker.sock:/var/run/docker.sock \
	  -v "$(CURDIR)/tests/docker.yml:/test.yml:ro" \
	  $(UT_IMAGE) test $(UTFLAGS) --image $(DOCKER_LOCAL):$(DOCKER_TAG) --config /test.yml

pull:
	docker pull $(DOCKERFLAGS) $(DOCKER_LOCAL):$(DOCKER_TAG)

push:
	docker push $(DOCKERFLAGS) $(DOCKER_LOCAL):$(DOCKER_TAG)

image-name: ## print the docker image name
	@echo -n "$(DOCKER_LOCAL):$(DOCKER_TAG)"

.PHONY: image-name-version
KUBECTL_VERSION=$(shell docker run --rm "$(DOCKER_LOCAL):$(DOCKER_TAG)" kubectl version --short=true --client=true | sed 's/^C.*: v//')
image-name-version:
	@echo -n "$(DOCKER_LOCAL):$(DOCKER_TAG)-$(KUBECTL_VERSION)"

#-------------------------------------------------------------------------------

