FROM julienlecomte/docker-make

RUN KUBECTL_VERSION=$(curl -sS https://storage.googleapis.com/kubernetes-release/release/stable.txt) \
    && wget -q "https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl" -O /tmp/kubectl \
    && install -p -m755 -oroot -groot /tmp/kubectl /usr/local/bin/ \
    && rm -f /tmp/kubectl \
    ;

