# docker-make-kubectl

A Docker image that contains just the basic tools to use Docker and Kubectl with Makefile or scripts.


## Building

Building is as easy as just running `make`, with tag being the branch name:

~~~bash
make
~~~

## Downloading

`docker-make-kubectl` can be downloaded from Docker Hub or GitLab:

~~~bash
docker pull julienlecomte/docker-make-kubectl
# or
docker pull registry.gitlab.com/jlecomte/images/docker-make-kubectl
~~~

## Contents

This docker inherits from [`julienlecomte/docker-make`](https://hub.docker.com/r/julienlecomte/docker-make), which inherits itself from `docker:git` and `docker:stable` and adds:
  * `kubectl`: to run commands against Kubernetes clusters

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Locations

  * GitLab: [https://gitlab.com/jlecomte/images/docker-make-kubectl](https://gitlab.com/jlecomte/images/docker-make-kubectl)
  * Docker hub: [https://hub.docker.com/r/julienlecomte/docker-make-kubectl](https://hub.docker.com/r/julienlecomte/docker-make-kubectl)

